import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import cv2
import numpy as np
from math import pi
image = mpimg.imread('exit-ramp.jpg')


gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

low_threshold = 0
high_threshold = 200

kernel_size=3
blur_gray = cv2.GaussianBlur(gray,(kernel_size, kernel_size),0)
edges = cv2.Canny(blur_gray, low_threshold, high_threshold)
rho = 1
theta = pi/180
threshold = 3
min_line_length = 9
max_line_gap = 5
lines = cv2.HoughLinesP(edges, rho , theta, threshold, np.array([]), min_line_length, max_line_gap)

line_image = np.copy(image)*0

#+BEGIN_SRC python
for line in lines:
  for x1,y1,x2,y2 in line:
    cv2.line(line_image,(x1,y1),(x2,y2),(255,0,0),10)
#+END_SRC
color_edges = np.dstack((edges, edges, edges))
combo = cv2.addWeighted(color_edges, 0.8, line_image, 1, 0) 

plt.imshow(combo)
plt.show()


