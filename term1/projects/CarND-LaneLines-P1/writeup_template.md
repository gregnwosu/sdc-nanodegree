
# Table of Contents

1.  [Finding Lane Lines on the Road\*\*](#orgcbd25c0)
    1.  [Pipeline description](#org2fe68a0)
    2.  [drawlines2](#orgf4aa9fc)
        1.  [gradient sorting](#org792c239)
        2.  [gradient selection](#org360ae58)
        3.  [line averaging /drawing](#org1494cf1)
    3.  [Reflection](#org641b542)
        1.  [possible improvements to pipeline](#org2283a9d)



<a id="orgcbd25c0"></a>

# Finding Lane Lines on the Road\*\*

The goals / steps of this project are the following:

-   Make a pipeline that finds lane lines on the road
-   Reflect on your work in a written report


<a id="org2fe68a0"></a>

## Pipeline description

My pipeline consisted of 7 steps.

1.  resized the image , as the image in the challenge is not the standard 960 , 540 the mask expects
    standard 960,540 siz
2.  convert the image to grayscale
3.  apply Gaussian blurring with a kernel size of 5 to remove as much
    noise as possible
4.  canny edge detect to find points of gradient change
5.  masked off the region of interest
6.  applied [houghlines](test_images_output/solidWhiteCurve.jpg) transform to connect canny points
7.  houghlines calls my [1.2](#orgf4aa9fc) function

    def pipeline(img):
        img =cv2.resize(img,(960,540))
        g = grayscale(img)
        b = gaussian_blur(g, kernel_size)
        c = canny(b, low_threshold, high_threshold)
        r = region_of_interest(c, vertices)
        h = hough_lines(r, rho, theta, threshold, min_line_len, max_line_gap)
        w = weighted_img(h,img)
        return w


<a id="orgf4aa9fc"></a>

## drawlines2


<a id="org792c239"></a>

### gradient sorting

essentially this function sorts the lines into those with a positive
or negative gradient to determine which lines are left leaning vs
right leaning. However it also tries to remove lines that have very
small (horizontal) gradients as a further filter to remove noise
. Below you can see how it filters lines within the mask area that are
flatter than -0.2 gradient

    ...
    positive_grads = [l for l in lines if grad(l) < -0.2 ]
    ..


<a id="org360ae58"></a>

### gradient selection

draw lines also keeps a track of the last negative and positive
gradients and **continues** to use an older gradient, if:

1.  a new gradient cannot be detected, or
2.  a new gradient differs wildly from the last gradient detected

This was added for stability for the challenge code

    def choosegrads(new_grads, old_grads):
        if len(new_grads) < 1 :
           return old_grads
        new_mean = (np.mean(np.array(new_grads), axis=0))
        old_mean = (np.mean(np.array(old_grads), axis=0))
        if abs(np.sum(new_mean - old_mean)) > 30:
            return old_grads
        return new_grads


<a id="org1494cf1"></a>

### line averaging /drawing

I use averaging to get a consensus from all the lines detected, by
using \`\`\`numpy.avg\`\`\` across axis 0 from the array of lines returned
by hough lines. I then extrapolate by working out the gradient and
algebraic manipulation of y= mx + b.
I calculate lines for my area of interest

    def grad(line , default=0.01):
        if (line.shape) != (1,4):
            return default
        x1,y1,x2,y2 = line[0]
        return (y2-y1)/(x2-x1)

    def extrapolate(line):
        m = grad(line)
        x,y,_,_ = line[0]
        b = y -(m * x)
        y1 = 320
        x1 = (y1-b)/m
        y2 = 540
        x2 = (y2-b)/m
        return x1,y1,x2,y2


<a id="org641b542"></a>

## Reflection

 I needed to create a more robust system and so I store the
  last gradient.
I have two issues with this:

1.  it is slightly inefficient computationally , i would be better to
    cache the extrapolated lines
2.  the cached gradients priority should decay after time, there should be some
    alert raised if it has been used for a very long period and not updated.


<a id="org2283a9d"></a>

### possible improvements to pipeline

1.  use python generators so that no so much is held in memory at a time
2.  Another potential improvement could be to implement a decay model
    for the cached gradient . see above
